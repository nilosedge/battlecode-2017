package swarms02;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TreeInfo;

public class GC {

	public static int id;
	public static double rand;
	public static int roundNum;

	public static RobotInfo[] allRobots;
	public static RobotInfo[] friendRobots;
	public static RobotInfo[] enemyRobots;

	public static TreeInfo[] allTrees;
	public static TreeInfo[] friendTrees;
	public static TreeInfo[] enemyTrees;
	public static TreeInfo[] neutTrees;

	public static ArrayList<TreeInfo> treesWithBullets;
	public static ArrayList<TreeInfo> treesWithUnits;

	public static MapLocation myLocation;
	public static Team myTeam;
	public static RobotType myType;
	public static Util util = new Util();


	public static void init(RobotController rc) {
		id = rc.getID();
		myType = rc.getType();
		myTeam = rc.getTeam();
	}

	public static void updateCounts(RobotController rc, Broadcast cast) throws GameActionException {
		//int start = Clock.getBytecodeNum();

		roundNum = rc.getRoundNum();
		rand = Math.random();
		myLocation = rc.getLocation();
		allRobots = rc.senseNearbyRobots();
		enemyRobots = rc.senseNearbyRobots(myLocation, myType.sensorRadius, myTeam.opponent());
		friendRobots = rc.senseNearbyRobots(myLocation, myType.sensorRadius, myTeam);
		
		if(enemyRobots.length > 0) {
			cast.setCurrentEnemyLocation(enemyRobots[0].getLocation());
		}

		allTrees = rc.senseNearbyTrees();
		enemyTrees = rc.senseNearbyTrees(myLocation, myType.sensorRadius, myTeam.opponent());
		friendTrees = rc.senseNearbyTrees(myLocation, myType.sensorRadius, myTeam);
		neutTrees = rc.senseNearbyTrees(myLocation, myType.sensorRadius, Team.NEUTRAL);
		treesWithBullets = new ArrayList<>();
		treesWithUnits = new ArrayList<>();
		for(TreeInfo info: neutTrees) {
			if(info.getContainedBullets() > 0) {
				treesWithBullets.add(info);
			}
			if(info.getContainedRobot() != null) {
				treesWithUnits.add(info);
			}
		}

		if(enemyTrees.length > 0) {
			cast.setCurrentTreeLocation(enemyTrees[0].getLocation());
		}
		
		//int end = Clock.getBytecodeNum();
		//System.out.println("BCU: Init: " + (end - start));
	}

}
