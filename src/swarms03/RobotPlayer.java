package swarms03;

import battlecode.common.RobotController;

public class RobotPlayer {
	
	public static void run(RobotController rc) {
		BaseRobot robot = null;

		try {
			switch(rc.getType()) {
			case ARCHON:
				robot = new ArchonRobot(rc);
				break;
			case GARDENER:
				robot = new GardenerRobot(rc);
				break;
			case LUMBERJACK:
				robot = new LumberJackRobot(rc);
				break;
			case SCOUT:
				robot = new ScoutRobot(rc);
				break;
			case SOLDIER:
				robot = new SoldierRobot(rc);
				break;
			case TANK:
				robot = new TankRobot(rc);
				break;
			default:
				break;
			}
			robot.loop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
