package swarms02;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.TreeInfo;

public class SoldierRobot extends BaseRobot {

	private MapLocation swarmPoint;
	private int shooter = 0;

	public SoldierRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {

		boolean dodge = dodge();
		shake();
		swarm();
		if(!dodge) swarmMove(swarmPoint);
		//normalMove();
		
		if(GC.enemyRobots.length > 0) {
			shoot(GC.enemyRobots[0].getLocation());
		} else {
			if(swarmPoint != null && GC.myLocation.distanceTo(swarmPoint) < 12) {
				shoot(swarmPoint);
			}
		}
	}

	private void shake() throws GameActionException {
		if(GC.treesWithBullets.size() > 0) {
			TreeInfo nearestNeutTree = GC.treesWithBullets.get(0);
			cast.setCurrentBulletTreeLocation(nearestNeutTree.getLocation());
			if(rc.canShake(nearestNeutTree.getLocation())) {
				rc.shake(nearestNeutTree.getLocation());
			}
		}
	}
	
	private void swarm() throws GameActionException {
		int id = cast.getSwarmLeader();
		if(id == 0) {
			swarmLeader();
		} else {
			swarmPoint = cast.getSwarmPoint();
		}
	}

	private void swarmLeader() throws GameActionException {
		swarmPoint = null;
		MapLocation swarmEnemy = cast.getSwarmEnemyPoint();
		MapLocation enemy = cast.getCurrentEnemyLocation();
		MapLocation enemyArchon = cast.getCurrentEnemyArchonLocation();

		if(swarmEnemy != null) {
			swarmPoint = swarmEnemy;
		} else if(enemyArchon != null) {
			swarmPoint = enemyArchon;
		} else if(enemy != null) {
			swarmPoint = enemy;
		}
		if(swarmPoint != null) {
			cast.setSwarmPoint(swarmPoint);
			cast.setSwarmShoot();
		}
		cast.setSwarmLeader(GC.id);
	}

	private void normalMove() throws GameActionException {
		MapLocation enemy = cast.getCurrentEnemyLocation();
		if(enemy != null) {
			mover.toward(enemy);
		} else {
			mover.explore();
		}
	}
	
	private void swarmMove(MapLocation swarmPoint) throws GameActionException {
		if(swarmPoint == null) {
			MapLocation enemy = cast.getCurrentEnemyLocation();
			if(enemy != null) {
				//System.out.println("swarmMove: mover.toward(enemy)");
				mover.toward(enemy);
			} else {
				mover.explore();
			}
		} else {
			if(GC.myLocation.distanceTo(swarmPoint) > 10) {
				//System.out.println("swarmMove: mover.toward(swarmPoint)");
				mover.toward(swarmPoint);
			} else if(GC.myLocation.distanceTo(swarmPoint) < 10) {
				mover.away(swarmPoint);
			} else {
				mover.explore();
			}
		}
	}

	private void shoot(MapLocation enemy) throws GameActionException {

		Direction d = GC.myLocation.directionTo(enemy);
		
		shooter = ++shooter % 3;
		if(shooter == 0) d = d.rotateLeftDegrees((float)6.6);
		if(shooter == 2) d = d.rotateRightDegrees((float)6.6);
		
		if(rc.canFirePentadShot()) {
			rc.firePentadShot(d);
		} else if(rc.canFireTriadShot()) {
			rc.fireTriadShot(d);
		} else if(rc.canFireSingleShot()) {
			rc.fireSingleShot(d);
		}

	}

	private boolean dodge() {
		return false;
	}

}
