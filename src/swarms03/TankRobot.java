package swarms03;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class TankRobot extends BaseRobot {

	public TankRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {
		
		MapLocation enemy = cast.getCurrentEnemyLocation();
		if(enemy != null) {
			mover.toward(enemy);
		} else {
			mover.explore();
		}
		
		shoot();

	}

	private void shoot() throws GameActionException {

		for(RobotInfo i: GC.enemyRobots) {
			Direction d = GC.myLocation.directionTo(i.getLocation());
			if(rc.canFireTriadShot()) {
				rc.fireTriadShot(d);
				break;
			}
			if(rc.canFireSingleShot()) {
				rc.fireSingleShot(d);
				break;
			}
		}
	}

}
