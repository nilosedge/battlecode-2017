package swarms01;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.TreeInfo;

public class ScoutRobot extends BaseRobot {

	Direction dir = new Direction((float)Math.random() * 2 * (float)Math.PI);
	
	public ScoutRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {
		
		if(StaticRoundInfo.enemyRobots.size() > 0) {
			shoot();
		} else if(StaticRoundInfo.treesWithBullets.size() > 0) {
			shake();
		} else {
			MapLocation location = cast.getCurrentBulletTreeLocation();
			if(location != null) {
				mover.toward(location);
			} else {
				changeDirection();
			}
		}
	}
	
	private void shake() throws GameActionException {
		
		TreeInfo nearestNeutTree = null;

		for(TreeInfo i: StaticRoundInfo.treesWithBullets) {
			if((nearestNeutTree == null || StaticRoundInfo.myLocation.distanceTo(i.getLocation()) < StaticRoundInfo.myLocation.distanceTo(nearestNeutTree.getLocation()))) {
				nearestNeutTree = i;
			}
		}

		if(nearestNeutTree != null) {
			cast.setCurrentBulletTreeLocation(nearestNeutTree.getLocation());
			if(rc.canShake(nearestNeutTree.getLocation())) {
				rc.shake(nearestNeutTree.getLocation());
			} else {
				mover.toward(nearestNeutTree.getLocation());
			}
		}
	}

	private void changeDirection() throws GameActionException {
		for(int i = 0; i < 6; i++) {
			Direction moved = mover.tryMove(dir);
			if(moved == null) {
				dir = dir.rotateRightDegrees(265);
			} else {
				dir = moved;
				break;
			}
		}
	}

	private void shoot() throws GameActionException {
		mover.toward(StaticRoundInfo.enemyRobots.get(0).getLocation());
		
		for(RobotInfo i: StaticRoundInfo.enemyRobots) {
			Direction d = rc.getLocation().directionTo(i.getLocation());
			if(rc.canFireSingleShot()) {
				rc.fireSingleShot(d);
				break;
			}
		}
	}

}
