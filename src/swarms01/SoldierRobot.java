package swarms01;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class SoldierRobot extends BaseRobot {

	private MapLocation swarmPoint;

	public SoldierRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {

		boolean dodge = dodge();
		swarm();
		if(!dodge) swarmMove(swarmPoint);
		//normalMove();
		
		if(StaticRoundInfo.enemyRobots.size() > 0) {
			shoot(StaticRoundInfo.enemyRobots.get(0).getLocation());
		} else {
			if(swarmPoint != null && rc.getLocation().distanceTo(swarmPoint) < 12) {
				shoot(swarmPoint);
			}
		}
	}

	private void swarm() throws GameActionException {
		int id = cast.getSwarmLeader();
		if(id == 0) {
			swarmLeader();
		} else {
			swarmPoint = cast.getSwarmPoint();
		}
	}

	private void swarmLeader() throws GameActionException {
		cast.setSwarmLeader(rc.getID());
		swarmPoint = null;
		MapLocation swarmEnemy = cast.getSwarmEnemyPoint();
		MapLocation enemy = cast.getCurrentEnemyLocation();
		MapLocation enemyArchon = cast.getCurrentEnemyArchonLocation();

		if(swarmEnemy != null) {
			swarmPoint = swarmEnemy;
		} else if(enemyArchon != null) {
			swarmPoint = enemyArchon;
		} else if(enemy != null) {
			swarmPoint = enemy;
		}
		if(swarmPoint != null) {
			cast.setSwarmPoint(swarmPoint);
			cast.setSwarmShoot();
		}
		cast.setSwarmLeader(id);
	}

	private void normalMove() throws GameActionException {
		MapLocation enemy = cast.getCurrentEnemyLocation();
		if(enemy != null) {
			mover.toward(enemy);
		} else {
			mover.random();
		}
	}
	
	private void swarmMove(MapLocation swarmPoint) throws GameActionException {
		if(swarmPoint == null) {
			MapLocation enemy = cast.getCurrentEnemyLocation();
			if(enemy != null) {
				//System.out.println("swarmMove: mover.toward(enemy)");
				mover.toward(enemy);
			} else {
				mover.random();
			}
		} else {
			if(rc.getLocation().distanceTo(swarmPoint) > 10) {
				//System.out.println("swarmMove: mover.toward(swarmPoint)");
				mover.toward(swarmPoint);
			} else if(rc.getLocation().distanceTo(swarmPoint) < 10) {
				mover.away(swarmPoint);
			} else {
				mover.random();
			}
		}
	}

	private void shoot(MapLocation enemy) throws GameActionException {

		Direction d = rc.getLocation().directionTo(enemy);
		if(StaticRoundInfo.rand < .33) d = d.rotateLeftDegrees((float)6.6);
		if(StaticRoundInfo.rand > .66) d = d.rotateRightDegrees((float)6.6);

		if(rc.canFireTriadShot()) {
			rc.fireTriadShot(d);
		} else if(rc.canFireSingleShot()) {
			rc.fireSingleShot(d);
		}

	}

	private boolean dodge() {
		return false;
	}

}
