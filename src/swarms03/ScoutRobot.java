package swarms03;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TreeInfo;

public class ScoutRobot extends BaseRobot {

	Direction dir = new Direction((float)Math.random() * 2 * (float)Math.PI);

	public ScoutRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {

		if(GC.enemyRobots.length > 0) {
			for(RobotInfo i: GC.enemyRobots) {
				if(i.getType() == RobotType.GARDENER) {
					mover.toward(i.getLocation());
					Direction d = GC.myLocation.directionTo(i.getLocation());
					if(rc.canFireSingleShot()) {
						rc.fireSingleShot(d);
						break;
					}
				}
			}
		} else if(GC.treesWithBullets.size() > 0) {
			TreeInfo nearestNeutTree = GC.treesWithBullets.get(0);
			mover.toward(nearestNeutTree.getLocation());
			cast.setCurrentBulletTreeLocation(nearestNeutTree.getLocation());
			if(rc.canShake(nearestNeutTree.getLocation())) {
				rc.shake(nearestNeutTree.getLocation());
			}
		} else {
			MapLocation location = cast.getCurrentBulletTreeLocation();
			if(location != null) {
				mover.toward(location);
			} else {
				changeDirection();
			}
		}
	}



	private void changeDirection() throws GameActionException {
		for(int i = 0; i < 6; i++) {
			Direction moved = mover.tryMove(dir);
			if(moved == null) {
				dir = dir.rotateRightDegrees(265);
			} else {
				dir = moved;
				break;
			}
		}
	}

}
