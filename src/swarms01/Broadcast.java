package swarms01;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class Broadcast {

	private RobotController rc;
	private static int GARDENER_COUNT_BUILT = 40;
	private static int LUMBERJACK_COUNT_BUILT = 41;
	private static int SOLDIER_COUNT_BUILT = 42;
	private static int SCOUT_COUNT_BUILT = 43;

	private static int TREE_LOCATION = 50; // 2
	private static int TREE_LOCATION_ROUND = 52;
	private static int BULLET_TREE_LOCATION = 53; // 2
	private static int BULLET_TREE_LOCATION_ROUND = 55;

	private static int ENEMY_LOCATION = 60; // 2
	private static int ENEMY_LOCATION_ROUND = 62;

	private static int ENEMY_ARCHON_LOCATION = 70; // 2
	private static int ENEMY_ARCHON_LOCATION_ROUND = 72;

	private static int SCOUT_COUNT = 80;
	private static int SCOUT_COUNT_TMP = 81;

	private static int SWARM_LEADER = 90;
	private static int SWARM_POINT = 91; //2
	private static int SWARM_POINT_ROUND = 93;
	private static int SWARM_ENEMY_LOCATION = 94; //2
	private static int SWARM_ENEMY_LOCATION_ROUND = 96;
	
	private static int SWARM_SHOOT = 97;
	
	private static int ARCHON_LEADER = 100;

	public Broadcast(BaseRobot baseRobot) {
		rc = baseRobot.rc;
	}


	public void setArchonLeader(int id) throws GameActionException {
		rc.broadcast(ARCHON_LEADER, id);
	}
	public int getArchonLeader() throws GameActionException {
		return rc.readBroadcast(ARCHON_LEADER);
	}

	public void resetSwarm() throws GameActionException {
		rc.broadcast(SWARM_LEADER, 0);
		setLocation(new MapLocation(0, 0), SWARM_ENEMY_LOCATION, SWARM_ENEMY_LOCATION_ROUND);
	}
	public void setSwarmLeader(int id) throws GameActionException {
		rc.broadcast(SWARM_LEADER, id);
	}
	public int getSwarmLeader() throws GameActionException {
		return rc.readBroadcast(SWARM_LEADER);
	}
	public MapLocation getSwarmPoint() throws GameActionException {
		return getLocation(SWARM_POINT, SWARM_POINT_ROUND);
	}
	public void setSwarmPoint(MapLocation location) throws GameActionException {
		setLocation(location, SWARM_POINT, SWARM_POINT_ROUND);
	}
	public MapLocation getSwarmEnemyPoint() throws GameActionException {
		return getLocation(SWARM_ENEMY_LOCATION);
	}
	public void setSwarmShoot() throws GameActionException {
		rc.broadcast(SWARM_SHOOT, 1);
	}
	public boolean getSwarmShoot() throws GameActionException {
		return rc.readBroadcast(SWARM_SHOOT) == 1;
	}
	
	public void incSoldierBuiltCount(int amount) throws GameActionException {
		int count = rc.readBroadcast(SOLDIER_COUNT_BUILT);
		rc.broadcast(SOLDIER_COUNT_BUILT, count+amount);
	}
	public void incLumberJackBuiltCount(int amount) throws GameActionException {
		int count = rc.readBroadcast(LUMBERJACK_COUNT_BUILT);
		rc.broadcast(LUMBERJACK_COUNT_BUILT, count+amount);
	}
	public void incGardenerBuiltCount(int amount) throws GameActionException {
		int count = rc.readBroadcast(GARDENER_COUNT_BUILT);
		rc.broadcast(GARDENER_COUNT_BUILT, count+amount);
	}
	public void incScoutBuiltCount(int amount) throws GameActionException {
		int count = rc.readBroadcast(SCOUT_COUNT_BUILT);
		rc.broadcast(SCOUT_COUNT_BUILT, count+amount);
	}

	public int getScoutBuiltCount() throws GameActionException {
		return rc.readBroadcast(SCOUT_COUNT_BUILT);
	}
	public int getSoldierBuiltCount() throws GameActionException {
		return rc.readBroadcast(SOLDIER_COUNT_BUILT);
	}
	public int getGardenerBuiltCount() throws GameActionException {
		return rc.readBroadcast(GARDENER_COUNT_BUILT);
	}
	public int getLumberJackBuiltCount() throws GameActionException {
		return rc.readBroadcast(LUMBERJACK_COUNT_BUILT);
	}

	public void setCurrentTreeLocation(MapLocation location) throws GameActionException {
		setLocation(location, TREE_LOCATION, TREE_LOCATION_ROUND);
	}
	public void setCurrentBulletTreeLocation(MapLocation location) throws GameActionException {
		setLocation(location, BULLET_TREE_LOCATION, BULLET_TREE_LOCATION_ROUND);
	}
	public void setCurrentEnemyLocation(MapLocation location) throws GameActionException {
		setLocation(location, ENEMY_LOCATION, ENEMY_LOCATION_ROUND);
	}
	public void setCurrentEnemyArchonLocation(MapLocation location) throws GameActionException {
		setLocation(location, ENEMY_ARCHON_LOCATION, ENEMY_ARCHON_LOCATION_ROUND);
	}
	
	private void setLocation(MapLocation location, int locationType, int locationRound) throws GameActionException {
		int x = Float.floatToIntBits(location.x);
		int y = Float.floatToIntBits(location.y);
		rc.broadcast(locationType, x);
		rc.broadcast(locationType + 1, y);
		if(locationRound > 0) {
			rc.broadcast(locationRound, rc.getRoundNum());
		}
	}

	public MapLocation getCurrentEnemyLocation() throws GameActionException {
		return getLocation(ENEMY_LOCATION, ENEMY_LOCATION_ROUND);
	}
	public MapLocation getCurrentTreeLocation() throws GameActionException {
		return getLocation(TREE_LOCATION, TREE_LOCATION_ROUND, 10);
	}
	public MapLocation getCurrentBulletTreeLocation() throws GameActionException {
		return getLocation(BULLET_TREE_LOCATION, BULLET_TREE_LOCATION_ROUND);
	}
	public MapLocation getCurrentEnemyArchonLocation() throws GameActionException {
		return getLocation(ENEMY_ARCHON_LOCATION, ENEMY_ARCHON_LOCATION_ROUND);
	}



	private MapLocation getLocation(int locationType) throws GameActionException {
		return getLocation(locationType, 0, 0);
	}
	private MapLocation getLocation(int locationType, int locationRound) throws GameActionException {
		return getLocation(locationType, locationRound, 5);
	}
	private MapLocation getLocation(int locationType, int locationRound, int timeout) throws GameActionException {

		MapLocation m = new MapLocation(
				Float.intBitsToFloat(rc.readBroadcast(locationType)),
				Float.intBitsToFloat(rc.readBroadcast(locationType + 1)))
				;
		if(locationRound > 0 && timeout > 0) {
			int round = rc.readBroadcast(locationRound);
			if(rc.getRoundNum() - round <= timeout) {
				return m;
			} else {
				return null;
			}
		} else {
			if(m.x == 0 && m.y == 0) {
				return null;
			} else {
				return m;
			}
		}
	}

	public double getLumberJackToGardenerRatio() throws GameActionException {
		int gc = getGardenerBuiltCount();
		int lc = getLumberJackBuiltCount();
		double ratio = 0;
		if(gc > 0) {
			ratio = (double)lc / (double)gc;
		}
		return ratio;
	}

	public double getSoldierToLumberJackRatio() throws GameActionException {
		int sc = getSoldierBuiltCount();
		int lc = getLumberJackBuiltCount();
		double ratio = 0;
		if(lc > 0) {
			ratio = (double)sc / (double)lc;
		} else {
			ratio = sc;
		}
		return ratio;
	}

}
