package swarms01;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.TreeInfo;

public class LumberJackRobot extends BaseRobot {

	public LumberJackRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {

		if(StaticRoundInfo.enemyRobots.size() > 0) { 
			attack();
		} else if(StaticRoundInfo.enemyTrees.size() > 0) {
			chopTrees();
		} else {
			MapLocation location = cast.getCurrentTreeLocation();
			if(location != null) {
				mover.toward(location);
			} else {
				mover.random();
			}
		}
	}

	private void attack() throws GameActionException {
		boolean attack = false;
		for(RobotInfo info: StaticRoundInfo.enemyRobots) {
			if(info.getLocation().distanceTo(rc.getLocation()) <= 2) {
				if(rc.canStrike()) {
					//rc.strike();
					attack = true;
					break;
				}
			}
		}
		if(!attack) {
			//mover.toward(enemyRobots.get(0).getLocation());
		}
	}

	private void chopTrees() throws GameActionException {

		TreeInfo nearestNeutTree = null;

		for(TreeInfo i: StaticRoundInfo.enemyTrees) {
			if((nearestNeutTree == null || StaticRoundInfo.myLocation.distanceTo(i.getLocation()) < StaticRoundInfo.myLocation.distanceTo(nearestNeutTree.getLocation()))) {
				nearestNeutTree = i;
			}
		}

		if(nearestNeutTree != null) {
			cast.setCurrentTreeLocation(nearestNeutTree.getLocation());
			if(rc.canChop(nearestNeutTree.getLocation())) {
				rc.chop(nearestNeutTree.getLocation());
			} else {
				mover.toward(nearestNeutTree.getLocation());
			}
		}
	}

}
