package swarms03;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.TreeInfo;

public class LumberJackRobot extends BaseRobot {

	public LumberJackRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {
		shake();
		attack();
		chopTrees();
		if(!dodge()) move();
	}

	private void shake() throws GameActionException {
		if(GC.treesWithBullets.size() > 0) {
			TreeInfo nearestNeutTree = GC.treesWithBullets.get(0);
			cast.setCurrentBulletTreeLocation(nearestNeutTree.getLocation());
			if(rc.canShake(nearestNeutTree.getLocation())) {
				rc.shake(nearestNeutTree.getLocation());
			}
		}
	}

	private void attack() throws GameActionException {
		if(GC.enemyRobots.length > 0) {
			for(RobotInfo info: GC.enemyRobots) {
				if(info.getLocation().distanceTo(GC.myLocation) <= 4) {
					if(rc.canStrike()) {
						rc.strike();
						break;
					}
				}
			}
		}
	}

	private void chopTrees() throws GameActionException {

		MapLocation clearTree = cast.getClearTree();

		if(clearTree != null && rc.canChop(clearTree)) {
			rc.chop(clearTree);
		} else {
			TreeInfo nearestNeutTree = null;

			if(GC.neutTrees.length > 0) {
				nearestNeutTree = GC.neutTrees[0];
			}
			if(GC.enemyTrees.length > 0) {
				nearestNeutTree = GC.enemyTrees[0];
			}

			if(nearestNeutTree != null) {
				cast.setCurrentTreeLocation(nearestNeutTree.getLocation());
				if(rc.canChop(nearestNeutTree.getLocation())) {
					rc.chop(nearestNeutTree.getLocation());
				}
			}
		}

	}

	private void move() throws GameActionException {
		MapLocation clearTree = cast.getClearTree();
		if((GC.enemyRobots.length > 0 && GC.friendRobots.length > 10)) {
			mover.toward(GC.enemyRobots[0].getLocation());
		} else if(clearTree != null) {
			mover.toward(clearTree);
		} else if(GC.neutTrees.length > 0) {
			mover.toward(GC.neutTrees[0].getLocation());
		} else {
			MapLocation location = cast.getCurrentTreeLocation();
			if(location != null) {
				mover.toward(location);
			} else {
				mover.explore();
			}
		}
	}
}
