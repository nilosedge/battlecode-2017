package swarms02;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public abstract class BaseRobot {
	
	public RobotController rc;
	public MoveManager mover;
	public Broadcast cast;

	// Default constructor
	public BaseRobot(RobotController myRC) {
		rc = myRC;
		mover = new MoveManager(this);
		cast = new Broadcast(this);
	}
	
	abstract public void run() throws GameActionException;
	
	public void loop() {
		GC.init(rc);
		while (true) {
			try {
				GC.updateCounts(rc, cast);
				run();
				donate();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Clock.yield();
		}
	}

	private void donate() throws GameActionException {
		if(rc.getTeamBullets() > 1000 && GC.roundNum > 1000) {
			rc.donate(100);
		}
	}
}