package swarms02;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TreeInfo;

public class Util {

	private MapLocation myLocation;
	private RobotType myType;
	private RobotInfo[] friendRobots;
	private RobotInfo[] enemyRobots;
	private TreeInfo[] allTrees;
	
	private List<String> roundInfos = new ArrayList<String>();

	public void setLocation(MapLocation myLocation) {
		this.myLocation = myLocation;
	}

	public void setType(RobotType myType) {
		this.myType = myType;
	}
	public void setFriends(RobotInfo[] friendRobots) {
		this.friendRobots = friendRobots;
	}
	public void setEnemies(RobotInfo[] enemyRobots) {
		this.enemyRobots = enemyRobots;
	}
	public void setTrees(TreeInfo[] allTrees) {
		this.allTrees = allTrees;
	}

	public String roundInfo(int round) {
		StringBuffer buf = new StringBuffer();
		buf.append("{");
		buf.append("\"roundNum\": " + round + ",");
		buf.append("\"robot\": " + robotInfo(myLocation, myType) + ",");
		buf.append("\"friends\": [");
		String delim = "";
		for(RobotInfo info: friendRobots) {
			buf.append(delim + robotInfo(info.location, info.type));
			delim = ",";
		}
		buf.append("],");
		
		buf.append("\"enemies\": [");
		delim = "";
		for(RobotInfo info: enemyRobots) {
			buf.append(delim + robotInfo(info.location, info.type));
			delim = ",";
		}
		buf.append("],");
		
		delim = "";
		buf.append("\"trees\": [");
		for(TreeInfo info: allTrees) {
			buf.append(delim + treeInfo(info.location, info.radius));
			delim = ",";
		}
		buf.append("]");
		buf.append("}");
		return buf.toString();
	}

	private String treeInfo(MapLocation location, float bodyRadius) {
		StringBuffer buf = new StringBuffer();
		buf.append("{");
		buf.append("\"radius\": " + bodyRadius + ",");
		buf.append(location(location));
		buf.append("}");
		return buf.toString();
	}
	
	private String robotInfo(MapLocation location, RobotType type) {
		StringBuffer buf = new StringBuffer();
		buf.append("{");
		buf.append("\"bodyRadius\": " + type.bodyRadius + ",");
		buf.append("\"bulletSightRadius\": " + type.bulletSightRadius + ",");
		buf.append("\"sensorRadius\": " + type.sensorRadius + ",");
		buf.append("\"strideRadius\": " + type.strideRadius + ",");
		buf.append(location(location));
		buf.append("}");
		return buf.toString();
	}

	private String location(MapLocation location) {
		StringBuffer buf = new StringBuffer();
		buf.append("\"location\": {");
		buf.append("\"x\": " + location.x + ",");
		buf.append("\"y\": " + location.y);
		buf.append("}");
		return buf.toString();
	}

	public void appendRoundInfo(int round) {
		roundInfos.add(roundInfo(round));
	}

	public String getGame() {
		StringBuffer buf = new StringBuffer();
		buf.append("{");
		buf.append("\"rounds\": [");
		buf.append(String.join(",", roundInfos));
		buf.append("]");
		buf.append("}");
		return buf.toString();
	}
}
