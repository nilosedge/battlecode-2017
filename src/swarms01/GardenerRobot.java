package swarms01;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TreeInfo;

public class GardenerRobot extends BaseRobot {

	public GardenerRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {
		if(!buildArmy() && !buildTrees() && !waterTrees()) {
			mover.random();
		}
	}

	private boolean waterTrees() throws GameActionException {
		TreeInfo lowestTree = null;

		if(StaticRoundInfo.friendTrees.size() > 0) {
			for(TreeInfo i: StaticRoundInfo.friendTrees) {
				if(lowestTree == null || i.getHealth() < lowestTree.getHealth()) {
					lowestTree = i;
				}
			}
		}

		if(lowestTree != null) {
			if(rc.canWater(lowestTree.getLocation())) {
				rc.water(lowestTree.getLocation());
				return true;
			}
		}
		return false;
	}

	private boolean buildArmy() throws GameActionException {
		Direction dir = new Direction(0);

		double ratio = cast.getLumberJackToGardenerRatio();
		double ratio2 = cast.getSoldierToLumberJackRatio();
		int scc = cast.getScoutBuiltCount();

		for(int i = 0; i < 6; i++) {
			dir = dir.rotateRightDegrees(60);
			if (rc.canBuildRobot(RobotType.SCOUT, dir) && (rc.getRoundNum() == 2 || scc == 0)) {
				rc.buildRobot(RobotType.SCOUT, dir);
				cast.incScoutBuiltCount(1);
				return true;
			} else if (rc.canBuildRobot(RobotType.SOLDIER, dir) && ratio2 < 1 && cast.getLumberJackBuiltCount() > 0) {
				rc.buildRobot(RobotType.SOLDIER, dir);
				cast.incSoldierBuiltCount(1);
				return true;
			} else if (rc.canBuildRobot(RobotType.LUMBERJACK, dir) && ratio < 2) {
				rc.buildRobot(RobotType.LUMBERJACK, dir);
				cast.incLumberJackBuiltCount(1);
				return true;
			}
		}
		return false;
	}

	private boolean buildTrees() throws GameActionException {
		Direction dir;
		if(rc.getTeam() == Team.A) {
			dir = new Direction(0);
		} else {
			dir = new Direction((float)Math.PI);
		}

		for(int i = 0; i < 5; i++) {
			dir = dir.rotateRightRads((float)(Math.PI / 3));
			if(rc.canPlantTree(dir)) {
				rc.plantTree(dir);
				return true;
			}
		}
		return false;
	}


}
