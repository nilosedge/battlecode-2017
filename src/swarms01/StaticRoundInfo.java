package swarms01;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.TreeInfo;

public class StaticRoundInfo {

	public static int id;
	public static double rand;

	public static RobotInfo[] allRobots;
	public static ArrayList<RobotInfo> friendRobots = new ArrayList<>();
	public static ArrayList<RobotInfo> enemyRobots = new ArrayList<>();
	
	public static TreeInfo[] allTrees;
	public static ArrayList<TreeInfo> friendTrees = new ArrayList<>();
	public static ArrayList<TreeInfo> enemyTrees = new ArrayList<>();
	
	public static ArrayList<TreeInfo> treesWithBullets = new ArrayList<>();

	public static MapLocation myLocation;
	
	public static void updateCounts(RobotController rc, Broadcast cast) throws GameActionException {
		id = rc.getID();
		rand = Math.random();
		myLocation = rc.getLocation();
		allRobots = rc.senseNearbyRobots();
		enemyRobots.clear();
		friendRobots.clear();
		for(RobotInfo info: allRobots) {
			if(info.getTeam() != rc.getTeam()) {
				enemyRobots.add(info);
			} else {
				friendRobots.add(info);
			}
		}
		if(enemyRobots.size() > 0) {
			cast.setCurrentEnemyLocation(enemyRobots.get(0).getLocation());
		}
		
		allTrees = rc.senseNearbyTrees();
		enemyTrees.clear();
		friendTrees.clear();
		treesWithBullets.clear();
		for(TreeInfo info: allTrees) {
			if(info.getTeam() != rc.getTeam()) {
				enemyTrees.add(info);
			} else {
				friendTrees.add(info);
			}
			if(info.getContainedBullets() > 0) {
				treesWithBullets.add(info);
			}
		}
		if(enemyTrees.size() > 0) {
			cast.setCurrentTreeLocation(enemyTrees.get(0).getLocation());
		}
	}

}
