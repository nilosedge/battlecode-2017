package swarms03;

import java.util.List;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.TreeInfo;

public class SoldierRobot extends BaseRobot {

	private MapLocation swarmPoint;
	private List<RadianPair> unshootables;

	public SoldierRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {
		
		unshootables = GC.getUnshootables();
		
		boolean dodge = dodge();
		shake();
		swarm();
		
		if(!dodge) swarmMove(swarmPoint);
		
		if(GC.enemyRobots.length > 0) {
			Direction toEnemy = new Direction(0);
			int amount = 0;
			for(RobotInfo info: GC.enemyRobots) {
				amount = canShoot(GC.myLocation.directionTo(info.location));
				if(amount > 0) {
					toEnemy = GC.myLocation.directionTo(info.location);
					break;
				}
			}
			if(amount > 0) {
				shoot(amount, toEnemy);
			}
		} else if(swarmPoint != null && GC.myLocation.distanceTo(swarmPoint) < 15) {
			Direction toEnemy = GC.myLocation.directionTo(swarmPoint);
			int amount = canShoot(toEnemy);
			if(amount > 0) {
				shoot(amount, toEnemy);
			}
		}
	}

	private void shoot(int amount, Direction d) throws GameActionException {
		if(amount == 5 && rc.canFirePentadShot()) {
			rc.firePentadShot(d);
		} else if(amount == 3 && rc.canFireTriadShot()) {
			rc.fireTriadShot(d);
		} else if(amount == 1 && rc.canFireSingleShot()) {
			rc.fireSingleShot(d);
		}
	}

	private void shake() throws GameActionException {
		if(GC.treesWithBullets.size() > 0) {
			TreeInfo nearestNeutTree = GC.treesWithBullets.get(0);
			cast.setCurrentBulletTreeLocation(nearestNeutTree.getLocation());
			if(rc.canShake(nearestNeutTree.getLocation())) {
				rc.shake(nearestNeutTree.getLocation());
			}
		}
	}
	
	private void swarm() throws GameActionException {
		int id = cast.getSwarmLeader();
		if(id == 0) {
			swarmLeader();
		} else {
			swarmPoint = cast.getSwarmPoint();
		}
	}

	private void swarmLeader() throws GameActionException {
		swarmPoint = null;
		MapLocation swarmEnemy = cast.getSwarmEnemyPoint();
		MapLocation enemy = cast.getCurrentEnemyLocation();
		MapLocation enemyArchon = cast.getCurrentEnemyArchonLocation();

		if(swarmEnemy != null) {
			swarmPoint = swarmEnemy;
		} else if(enemyArchon != null) {
			swarmPoint = enemyArchon;
		} else if(enemy != null) {
			swarmPoint = enemy;
		}
		if(swarmPoint != null) {
			cast.setSwarmPoint(swarmPoint);
			cast.setSwarmShoot();
		}
		cast.setSwarmLeader(GC.id);
	}
	
	private void swarmMove(MapLocation swarmPoint) throws GameActionException {
		if(swarmPoint == null) {
			MapLocation enemy = cast.getCurrentEnemyLocation();
			if(enemy != null) {
				//System.out.println("swarmMove: mover.toward(enemy)");
				mover.toward(enemy);
			} else {
				mover.explore();
			}
		} else {
			if(GC.myLocation.distanceTo(swarmPoint) > 10) {
				//System.out.println("swarmMove: mover.toward(swarmPoint)");
				mover.toward(swarmPoint);
			} else if(GC.myLocation.distanceTo(swarmPoint) < 10) {
				mover.away(swarmPoint);
			} else {
				mover.explore();
			}
		}
	}

	private int canShoot(Direction toEnemy) throws GameActionException {

		boolean pentadShot = true;
		boolean triadShot = true;
		boolean singleShot = true;
		for(RadianPair pair: unshootables) {
			if(pentadShot) {
				pentadShot &= pair.doesNotIntersect(toEnemy.rotateLeftDegrees(40), toEnemy.rotateRightDegrees(40));
			}
			if(triadShot) {
				triadShot &= pair.doesNotIntersect(toEnemy.rotateLeftDegrees(20), toEnemy.rotateRightDegrees(20));
			}
			if(singleShot) {
				singleShot &= pair.doesNotIntersect(toEnemy);
			}
		}
		
		if(pentadShot && triadShot && singleShot) {
			return 5;
		} else if(triadShot && singleShot) {
			return 3;
		} else if(singleShot) {
			return 1;
		} else {
			return 0;
		}
	}


}
