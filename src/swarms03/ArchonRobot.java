package swarms03;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TreeInfo;

public class ArchonRobot extends BaseRobot {

	public boolean archonLeader = false;
	
	public ArchonRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {

		// Calculate free space
		// Move away from obsticles
		cast.resetSwarm();
		//cast.resetClearTree();
		shake();
		archonLeader();
		buildGardener();
		move();
	}
	
	private void shake() throws GameActionException {
		if(GC.treesWithBullets.size() > 0) {
			TreeInfo nearestNeutTree = GC.treesWithBullets.get(0);
			cast.setCurrentBulletTreeLocation(nearestNeutTree.getLocation());
			if(rc.canShake(nearestNeutTree.getLocation())) {
				rc.shake(nearestNeutTree.getLocation());
			}
		}
	}

	private void archonLeader() throws GameActionException {
		int id = cast.getArchonLeader();
		if(id < GC.id) {
			archonLeader = false;
			cast.setArchonLeader(GC.id);
		} else if(id == GC.id) {
			archonLeader = true;
		}
	}

	private void move() throws GameActionException {
		boolean moved = dodge();
		if(!moved) moved = moveAway();
		if(!moved) mover.explore();
	}

	private boolean moveAway() throws GameActionException {
		for(RobotInfo i: GC.friendRobots) {
			if(GC.myLocation.distanceTo(i.getLocation()) < (GC.myType.bodyRadius * 2) || i.getType() == RobotType.GARDENER) {
				mover.away(i.getLocation());
				return true;
			}
		}
		return false;
	}

	private void buildGardener() throws GameActionException {

		double ratio = cast.getLumberJackToGardenerRatio();

		Direction buildDir;
		if(GC.myTeam == Team.A) {
			buildDir = Direction.WEST;
		} else {
			buildDir = Direction.EAST;
		}

		if(GC.roundNum == 1 && cast.getGardenerBuiltCount() == 0) {
			for(int i = 0; i < 360; i += 15) {
				if(rc.canBuildRobot(RobotType.GARDENER, buildDir)) {
					rc.buildRobot(RobotType.GARDENER, buildDir);
					cast.incGardenerBuiltCount(1);
					break;
				} else {
					rc.setIndicatorDot(GC.myLocation.add(buildDir), 255, 0, 0);
				}
				buildDir = buildDir.rotateRightDegrees(15);
			}

		} else if(GC.roundNum > 2 && ratio >= 1.5) {

			for(int i = 0; i < 360; i += 15) {
				if(rc.canBuildRobot(RobotType.GARDENER, buildDir) ) {
					rc.buildRobot(RobotType.GARDENER, buildDir);
					cast.incGardenerBuiltCount(1);
					break;
				} else {
					rc.setIndicatorDot(GC.myLocation.add(buildDir), 255, 0, 0);
				}
				buildDir = buildDir.rotateRightDegrees(15);
			}

		}

	}

}
