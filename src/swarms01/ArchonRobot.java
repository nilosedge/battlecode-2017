package swarms01;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class ArchonRobot extends BaseRobot {

	public boolean archonLeader = false;
	
	public ArchonRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {

		// Calculate free space
		// Move away from obsticles
		cast.resetSwarm();
		archonLeader();
		buildGardener();
		move();
		donate();
	}

	private void archonLeader() throws GameActionException {
		int id = cast.getArchonLeader();
		if(id < StaticRoundInfo.id) {
			archonLeader = false;
			cast.setArchonLeader(StaticRoundInfo.id);
		} else if(id == StaticRoundInfo.id) {
			archonLeader = true;
		}
	}

	private void move() throws GameActionException {
		RobotInfo[] robots = rc.senseNearbyRobots();
		boolean moved = false;
		for(RobotInfo i: robots) {
			if(StaticRoundInfo.myLocation.distanceTo(i.getLocation()) < (rc.getType().bodyRadius * 2) || i.getType() == RobotType.GARDENER) {
				mover.away(i.getLocation());
				moved = true;
				break;
			}
		}
		if(!moved) {
			mover.random();
		}
	}

	private void donate() throws GameActionException {
		if(rc.getTeamBullets() > 1000) {
			rc.donate(500);
		}
	}

	private void buildGardener() throws GameActionException {

		double ratio = cast.getLumberJackToGardenerRatio();

		Direction buildDir;
		if(rc.getTeam() == Team.A) {
			buildDir = Direction.WEST;
		} else {
			buildDir = Direction.EAST;
		}

		if(rc.getRoundNum() == 1 && cast.getGardenerBuiltCount() == 0) {
			for(int i = 0; i < 360; i += 15) {
				if(rc.canBuildRobot(RobotType.GARDENER, buildDir)) {
					rc.buildRobot(RobotType.GARDENER, buildDir);
					cast.incGardenerBuiltCount(1);
					break;
				} else {
					rc.setIndicatorDot(StaticRoundInfo.myLocation.add(buildDir), 255, 0, 0);
				}
				buildDir = buildDir.rotateRightDegrees(15);
			}

		} else if(rc.getRoundNum() > 2 && ratio >= 2) {

			for(int i = 0; i < 360; i += 15) {
				if(rc.canBuildRobot(RobotType.GARDENER, buildDir) ) {
					rc.buildRobot(RobotType.GARDENER, buildDir);
					cast.incGardenerBuiltCount(1);
					break;
				} else {
					rc.setIndicatorDot(StaticRoundInfo.myLocation.add(buildDir), 255, 0, 0);
				}
				buildDir = buildDir.rotateRightDegrees(15);
			}

		}

	}

}
