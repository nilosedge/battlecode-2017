package swarms03;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TreeInfo;

public class GardenerRobot extends BaseRobot {

	private Direction spawnDirection;
	private boolean treesBuilt = false;
	private boolean haveMoved = false;

	public GardenerRobot(RobotController myRC) {
		super(myRC);
	}

	@Override
	public void run() throws GameActionException {
		haveMoved = dodge();
		shake();
		buildArmy();
		buildTrees();
		waterTrees();
		updateClearTrees();
		if(!haveMoved && !treesBuilt) mover.explore();

	}

	private void shake() throws GameActionException {
		if(GC.treesWithBullets.size() > 0) {
			TreeInfo nearestNeutTree = GC.treesWithBullets.get(0);
			cast.setCurrentBulletTreeLocation(nearestNeutTree.getLocation());
			if(rc.canShake(nearestNeutTree.getLocation())) {
				rc.shake(nearestNeutTree.getLocation());
			}
		}
	}

	private void buildArmy() throws GameActionException {

		double ratio = cast.getLumberJackToGardenerRatio();
		double ratio2 = cast.getSoldierToLumberJackRatio();
		int scc = cast.getScoutBuiltCount();

		Direction actualSpawnDir = new Direction(0);

		if(spawnDirection == null) {
			for(int i = 0; i < 6; i++) {
				if(rc.canPlantTree(actualSpawnDir)) {
					break;
				} else {
					actualSpawnDir = actualSpawnDir.rotateRightDegrees(60);
				}
			}
		} else {
			actualSpawnDir = new Direction(spawnDirection.radians);
		}


		if (rc.canBuildRobot(RobotType.SOLDIER, actualSpawnDir) && ratio2 < 2 && cast.getLumberJackBuiltCount() > 0) {
			rc.buildRobot(RobotType.SOLDIER, actualSpawnDir);
			cast.incSoldierBuiltCount(1);
		} else if (rc.canBuildRobot(RobotType.LUMBERJACK, actualSpawnDir) && ratio < 1.5) {
			rc.buildRobot(RobotType.LUMBERJACK, actualSpawnDir);
			cast.incLumberJackBuiltCount(1);
		} else if (rc.canBuildRobot(RobotType.SCOUT, actualSpawnDir) && (GC.roundNum == 2 || scc == 0)) {
			rc.buildRobot(RobotType.SCOUT, actualSpawnDir);
			cast.incScoutBuiltCount(1);
		}

	}

	private void buildTrees() throws GameActionException {

		if(!toClose()) {
			if(GC.myTeam == Team.A) {
				spawnDirection = new Direction(0);
				for(int i = 0; i < 5; i++) {
					if(rc.canPlantTree(spawnDirection)) {
						break;
					} else {
						spawnDirection = spawnDirection.rotateLeftDegrees(60);
					}
				}
			} else {
				spawnDirection = new Direction((float)Math.PI);
				for(int i = 0; i < 5; i++) {
					if(rc.canPlantTree(spawnDirection)) {
						break;
					} else {
						spawnDirection = spawnDirection.rotateLeftDegrees(60);
					}
				}
			}

			Direction actualSpawnDir = new Direction(spawnDirection.radians);

			for(int i = 0; i < 5; i++) {
				actualSpawnDir = actualSpawnDir.rotateRightRads((float)(Math.PI / 3));
				if(rc.canPlantTree(actualSpawnDir)) {
					rc.plantTree(actualSpawnDir);
					treesBuilt = true;
				}
			}
		}
	}

	private boolean toClose() throws GameActionException {
		for(RobotInfo info: GC.friendRobots) {
			if(info.getType() == RobotType.GARDENER && GC.myLocation.distanceTo(info.getLocation()) < 6.5) {
				mover.away(info.getLocation());
				haveMoved = true;
				return true;
			}
		}
		return false;
	}

	private void waterTrees() throws GameActionException {
		TreeInfo lowestTree = null;

		if(GC.friendTrees.length > 0) {
			for(TreeInfo i: GC.friendTrees) {
				if(lowestTree == null || i.getHealth() < lowestTree.getHealth()) {
					lowestTree = i;
				}
			}
		}

		if(lowestTree != null) {
			if(rc.canWater(lowestTree.getLocation())) {
				rc.water(lowestTree.getLocation());
			}
		}
	}

	private void updateClearTrees() throws GameActionException {
		if(GC.neutTrees.length > 0) {
			if(GC.neutTrees[0].getLocation().distanceTo(GC.myLocation) < 4) {
				cast.setClearTree(GC.neutTrees[0].getLocation());
			}
		}
	}
}
