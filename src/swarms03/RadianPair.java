package swarms03;

import battlecode.common.Direction;

public class RadianPair {

	public Direction left;
	public Direction right;
	public RadianPair(Direction left, Direction right) {
		this.left = left;
		this.right = right;
	}
	
	public boolean doesNotIntersect(Direction shoot1, Direction shoot2) {
		return doesNotIntersect(shoot1) && doesNotIntersect(shoot2);
	}
	
	public boolean doesNotIntersect(Direction shoot) {
		return !intersect(shoot);
	}
	
	public boolean intersect(Direction shoot) {
		return shoot.radians <= left.radians && shoot.radians >= right.radians;
	}
}
