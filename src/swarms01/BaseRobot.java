package swarms01;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public abstract class BaseRobot {
	
	public RobotController rc;
	public MoveManager mover;
	public Broadcast cast;
	public int id;

	// Default constructor
	public BaseRobot(RobotController myRC) {
		rc = myRC;
		id = rc.getID();
		mover = new MoveManager(this);
		cast = new Broadcast(this);
	}
	
	abstract public void run() throws GameActionException;
	
	public void loop() {
		while (true) {
			try {
				StaticRoundInfo.updateCounts(rc, cast);
				run();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Clock.yield();
		}
	}

}