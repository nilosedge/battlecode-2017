package swarms02;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class MoveManager {

	private RobotController rc;
	private Direction lastMoved;

	public MoveManager(BaseRobot baseRobot) {
		rc = baseRobot.rc;
	}

	public void away(MapLocation location) throws GameActionException {

		Direction away = GC.myLocation.directionTo(location).opposite();
		Direction moved = tryMove(away);
		
		if(moved == null) {
			//System.out.println("Going to have to start pathing");
		}
	}
	
	public void toward(MapLocation location) throws GameActionException {

		Direction toward = GC.myLocation.directionTo(location);
		Direction moved = tryMove(toward);
		
		if(moved == null) {
			//System.out.println("Going to have to start pathing");
		}
	}
	

	public Direction tryMove(Direction dir) throws GameActionException {
		Direction d = tryMove(dir,(float)Math.PI/6,8);
		if(d != null) {
			lastMoved = d;
			GC.myLocation = rc.getLocation();
		}
		return d;
    }

    private Direction tryMove(Direction dir, float degreeOffset, int checksPerSide) throws GameActionException {

        // First, try intended direction
        if (rc.canMove(dir)) {
            rc.move(dir);
            return dir;
        }

        double random = Math.random();
        int currentCheck = 1;
        if(random > 0.5) currentCheck = -1;
        
        //System.out.println("Move: " + dir);
        
        while(currentCheck<=checksPerSide) {
        	
        	//System.out.println("Left: " + dir.rotateLeftRads(degreeOffset*currentCheck) + " Right: " + dir.rotateRightRads(degreeOffset*currentCheck));
        	
            // Try the offset of the left side
            if(rc.canMove(dir.rotateLeftRads(degreeOffset*currentCheck))) {
                rc.move(dir.rotateLeftRads(degreeOffset*currentCheck));
                return dir.rotateLeftRads(degreeOffset*currentCheck);
            }
            // Try the offset on the right side
            if(rc.canMove(dir.rotateRightRads(degreeOffset*currentCheck))) {
                rc.move(dir.rotateRightRads(degreeOffset*currentCheck));
                return dir.rotateRightRads(degreeOffset*currentCheck);
            }

            currentCheck++;
        }
        return null;
    }

	public void random() throws GameActionException {
		Direction d = new Direction((float)Math.random() * 2 * (float)Math.PI);
		tryMove(d);
	}

	public void explore() throws GameActionException {
		if(lastMoved != null) {
			tryMove(lastMoved);
		} else {
			random();
		}
	}

}
